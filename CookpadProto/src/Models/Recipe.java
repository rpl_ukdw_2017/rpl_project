package Models;

import static Models.DBControl.connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Deschia
 */
public class Recipe {

    private String name;
    private int id;
    private String author;
    private Feedback feedback;
    private String ingredients;
    private String category;
    private String steps;
    private String picPath;
    
    public Recipe(){
        
    }

    public Recipe(
            String author,
            String name,
            String category,
            String ingredients,
            String steps,
            String picPath) {
        this.author = author;
        this.name = name;
        this.ingredients = ingredients;
        this.category = category;
        this.steps = steps;
        this.picPath = picPath;
    }

    public Recipe(
            int id,
            String name,
            String category,
            String ingredients,
            String steps,
            String picPath) {
        this.id = id;
        this.name = name;
        this.ingredients = ingredients;
        this.category = category;
        this.steps = steps;
        this.picPath = picPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Feedback getFeedback() {
        return feedback;
    }

    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public void addRecipeToDatabase(Recipe r) {
        String sql
                = "INSERT INTO "
                + "recipe(author,name,category,ingredients,steps,date_added,pic_path)"
                + "VALUES(?,?,?,?,?,datetime('now','localtime'),?)";

        try (Connection conn = connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, r.getAuthor());
            pstmt.setString(2, r.getName());
            pstmt.setString(3, r.getCategory());
            pstmt.setString(4, r.getIngredients());
            pstmt.setString(5, r.getSteps());
            pstmt.setString(6, r.getPicPath());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void updateRecipeOnDatabase(Recipe r) {
        String sql
                = "update recipe set "
                + "name = ?,"
                + "category = ?,"
                + "ingredients = ?,"
                + "steps = ?,"
                + "pic_path = ?,"
                + "date_added = datetime('now','localtime')"
                + "WHERE id = " + r.getId();

        try (Connection conn = connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, r.getName());
            pstmt.setString(2, r.getCategory());
            pstmt.setString(3, r.getIngredients());
            pstmt.setString(4, r.getSteps());
            pstmt.setString(5, r.getPicPath());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteRecipeOnDatabase(int id) {
        String sql = "DELETE FROM recipe WHERE id = " + id;

        try (Connection conn = connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public ArrayList<Recipe> getRecipeFromDatabase() {
        Recipe recipe;
        ArrayList<Recipe> recipeList = new ArrayList<>();
        String sql = "SELECT * FROM recipe";

        try (Connection conn = connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            while(rs.next()){
                recipe = new Recipe();
                recipe.setId(rs.getInt("id"));
                recipe.setAuthor(rs.getString("author"));
                recipe.setName(rs.getString("name"));
                recipe.setCategory(rs.getString("category"));
                recipe.setIngredients(rs.getString("ingredients"));
                recipe.setSteps(rs.getString("steps"));
                recipe.setPicPath(rs.getString("pic_path"));
                recipeList.add(recipe);
            }
            return recipeList;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
    
    public ArrayList<Recipe> getRecipeFromDatabase(
            String name, 
            String category, 
            String ingredients) {
        if(name.equals("")) name = "%";
        if(category.equals("")) category = "%";
        if(ingredients.equals("")) ingredients = "%";
        Recipe recipe;
        ArrayList<Recipe> recipeList = new ArrayList<>();
        String sql = "SELECT * FROM recipe WHERE "
                + "name LIKE '%" + name + "%' AND "
                + "category LIKE '%" + category + "%' AND "
                + "ingredients LIKE '%" + ingredients + "%'";

        try (Connection conn = connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            while(rs.next()){
                recipe = new Recipe();
                recipe.setId(rs.getInt("id"));
                recipe.setAuthor(rs.getString("author"));
                recipe.setName(rs.getString("name"));
                recipe.setCategory(rs.getString("category"));
                recipe.setIngredients(rs.getString("ingredients"));
                recipe.setSteps(rs.getString("steps"));
                recipe.setPicPath(rs.getString("pic_path"));
                recipeList.add(recipe);
            }
            return recipeList;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
