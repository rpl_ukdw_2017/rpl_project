/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Deschia
 */
public class DBControl {

    public static void createNewDatabase() {

        String url = "jdbc:sqlite:C:/sqlite/db/test.db";

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void createNewTable() {
        // SQLite connection string
        String url = "jdbc:sqlite:C:/sqlite/db/test.db";

        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS `users` (\n"
                + "	`id` INTEGER PRIMARY KEY AUTOINCREMENT,\n"
                + "	`type` TEXT NOT NULL,\n"
                + "	`email` TEXT NOT NULL,\n"
                + "	`username` TEXT NOT NULL,\n"
                + "	`password` TEXT NOT NULL,\n"
                + "	`full_name` TEXT NOT NULL\n"
                + ");";

        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        // SQL statement for creating a new table
        sql = "CREATE TABLE IF NOT EXISTS `request` (\n"
                + "	`id` INTEGER PRIMARY KEY AUTOINCREMENT,\n"
                + "	`recipe_id` INTEGER,\n"
                + "	`type` TEXT NOT NULL,\n"
                + "	`author` TEXT NOT NULL,\n"
                + "	`name` TEXT NOT NULL,\n"
                + "	`category` TEXT NOT NULL,\n"
                + "	`ingredients` TEXT NOT NULL,\n"
                + "	`steps` TEXT NOT NULL,\n"
                + "	`date_added` TEXT NOT NULL\n"
                + ");";

        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        // SQL statement for creating a new table
        sql = "CREATE TABLE IF NOT EXISTS `recipe` (\n"
                + "	`id` INTEGER PRIMARY KEY AUTOINCREMENT,\n"
                + "	`author` TEXT NOT NULL,\n"
                + "	`name` TEXT NOT NULL,\n"
                + "	`category` TEXT NOT NULL,\n"
                + "	`ingredients` TEXT NOT NULL,\n"
                + "	`steps` TEXT NOT NULL,\n"
                + "	`pic_path` TEXT NOT NULL,\n"
                + "	`date_added` TEXT NOT NULL\n"
                + ");";

        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        // SQL statement for creating a new table
        sql = "CREATE TABLE IF NOT EXISTS rating (\n"
                + "    user_id   INTEGER NOT NULL,\n"
                + "    recipe_id INTEGER NOT NULL,\n"
                + "    value     INTEGER NOT NULL,\n"
                + "PRIMARY KEY(user_id,recipe_id)"
                + ");";

        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:C://sqlite/db/test.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
}
