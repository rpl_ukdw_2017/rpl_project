/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import static Models.DBControl.connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Deschia
 */
public class Request {

    public enum RequestType {
        ADD,
        DELETE,
        EDIT
    }
    private int id;
    private Recipe recipe;
    private RequestType requestType;
    
    public Request(){
        
    }

    public Request(Recipe r, RequestType rt) {
        this.recipe = r;
        this.requestType = rt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestID) {
        this.requestType = requestID;
    }

    public void addRequestToDatabase(Request r) {
        Recipe rc = r.getRecipe();
        String sql
                = "INSERT INTO request"
                + "(type,author,name,category,ingredients,steps,date_added)"
                + "VALUES(?,?,?,?,?,?,datetime('now','localtime'))";

        try (Connection conn = connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, r.getRequestType().toString());
            pstmt.setString(2, rc.getAuthor());
            pstmt.setString(3, rc.getName());
            pstmt.setString(4, rc.getCategory());
            pstmt.setString(5, rc.getIngredients());
            pstmt.setString(6, rc.getSteps());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public ArrayList<Request> getRequestFromDatabase(){
        Request rq;
        Recipe rc;
        ArrayList<Request> requestList = new ArrayList<>();
        String sql = "SELECT * FROM request";

        try (Connection conn = connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            while(rs.next()){
                rc = new Recipe();
                rc.setId(rs.getInt("recipe_id"));
                rc.setAuthor(rs.getString("author"));
                rc.setName(rs.getString("name"));
                rc.setCategory(rs.getString("category"));
                rc.setIngredients(rs.getString("ingredients"));
                rc.setSteps(rs.getString("steps"));
                rq = new Request();
                rq.setId(rs.getInt("id"));
                rq.setRecipe(rc);
                rq.setRequestType(
                        RequestType.valueOf(rs.getString("type")));
                requestList.add(rq);
            }
            return requestList;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public void doRequest(Request rq){
        Recipe rc = rq.getRecipe();
        switch(rq.getRequestType()){
            case ADD:
                rc.addRecipeToDatabase(rc);
                break;
            case EDIT:
                break;
            case DELETE:
//                rc.deleteRecipeOnDatabase(rc);
                break;
            default:
                break;
        }
        this.deleteRequestFromDatabase(rq);
    }

    public void deleteRequestFromDatabase(Request rq) {
        String sql = "DELETE FROM request WHERE id = " + rq.getId();

        try (Connection conn = connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
