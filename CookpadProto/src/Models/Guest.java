/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import static Models.DBControl.connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Deschia
 */
public class Guest {

    public void register(
            String email,
            String password,
            String username,
            String fullName) {
        String sql =
                "INSERT INTO users(email,password,username,full_name)"
                + "VALUES(?,?,?,?)";
        String hash = 
                org.apache.commons.codec.digest.DigestUtils.md5Hex(password);

        try (Connection conn = connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, email);
            pstmt.setString(2, hash);
            pstmt.setString(3, username);
            pstmt.setString(4, fullName);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
