/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import static Models.DBControl.connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Deschia
 */
public class User {

    public int id;
    public String name;
    public String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean register(
            String email,
            String password,
            String username,
            String fullName) {
        String sql
                = "INSERT INTO users(type,email,password,username,full_name)"
                + "VALUES('member',?,?,?,?)";
        String hash
                = org.apache.commons.codec.digest.DigestUtils.md5Hex(password);

        try (Connection conn = connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, email);
            pstmt.setString(2, hash);
            pstmt.setString(3, username);
            pstmt.setString(4, fullName);
            pstmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
    
    public String login(String username, String password){
        String hash
                = org.apache.commons.codec.digest.DigestUtils.md5Hex(password);
        String sql = "SELECT * FROM users WHERE username = '" + username
                + "' AND password = '" + hash+"'";
        
        try (Connection conn = connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            while (rs.next()){
                return rs.getString("full_name");
            }
            return null;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

}
