package Cookpad;

import Controller.RecipeController;
import Models.*;
import static Models.DBControl.createNewDatabase;
import static Models.DBControl.createNewTable;
import java.util.ArrayList;
import javax.swing.UIManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Deschia
 */
public class Cookpad {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
        }
        
        createNewDatabase();
        createNewTable();
        
        RecipeController recipeController = new RecipeController();
        recipeController.startApplication();
//        Recipe rc = new Recipe();
//        ArrayList<Recipe> a = new ArrayList<>();
//        a = rc.getRecipeFromDatabase("", "", "");
//        a = recipeController.getRecipeFromDatabase();
//        Request r = new Request();
//        a = r.getRequestFromDatabase();
    }

}
