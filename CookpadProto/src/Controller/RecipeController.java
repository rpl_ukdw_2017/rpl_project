package Controller;

import Models.*;
import View.MainView;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Deschia
 */
public class RecipeController {

    public void requestPublishRecipe(
            String author,
            String name,
            String category,
            String ingredients,
            String steps,
            String picPath) {
        Recipe r = new Recipe(author, name, category, ingredients, steps, picPath);
//        Request request = new Request(recipe,ADD);
//        request.addRequestToDatabase(request);
        r.addRecipeToDatabase(r);
    }

    public void requestUpdateRecipe() {

    }

    public void requestDeleteRecipe(int id) {
//        Request rq = new Request(rc,DELETE);
//        rq.addRequestToDatabase(rq);
        Recipe r = new Recipe();
        r.deleteRecipeOnDatabase(id);
    }

    public void startApplication() {
        MainView mv = MainView.getInstance();
        mv.setVisible(true);
    }

    public void updateRequestList() {

    }

    public ArrayList<Recipe> getRecipeFromDatabase() {
        Recipe r = new Recipe();
        return r.getRecipeFromDatabase();
    }
    
    public ArrayList<Recipe> getRecipeFromDatabase(
            String name, 
            String category, 
            String ingredients) {
        Recipe r = new Recipe();
        return r.getRecipeFromDatabase(name,category,ingredients);
    }

    public void updateRecipeOnDatabase(
            int id,
            String name,
            String category,
            String ingredients,
            String steps,
            String picPath) {
        Recipe r = new Recipe(id, name, category, ingredients, steps, picPath);
        r.updateRecipeOnDatabase(r);
    }
    
}
