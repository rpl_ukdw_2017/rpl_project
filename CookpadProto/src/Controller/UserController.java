/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.User;

/**
 *
 * @author Deschia
 */
public class UserController {

    public boolean register(
            String email,
            String password,
            String username,
            String fullName) {
        User user = new User();
        return user.register(email, password, username, fullName);
    }
    
    public String login(String username, String password){
        User u = new User();
        return u.login(username, password);
    }
    
    public void logout(){
        
    }
}
